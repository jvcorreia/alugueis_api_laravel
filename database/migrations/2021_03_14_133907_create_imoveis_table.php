<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImoveisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imoveis', function (Blueprint $table) {
            $table->id();
            $table->integer('escriture')->unique();
            $table->string('cep');
            $table->string('uf');
            $table->string('city');
            $table->string('address');
            $table->integer('size');
            $table->string('photo')->nullable();
            $table->unsignedBigInteger('locador_id');
            $table->timestamps();
            $table->foreign('locador_id')->references('id')->on('locadores');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imoveis');
    }
}
