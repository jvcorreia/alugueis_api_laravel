<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAluguelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alugueis', function (Blueprint $table) {
            $table->id();
            $table->float('price', 8, 2);
            $table->integer('location_type');
            $table->unsignedBigInteger('locatario_id')->unique();
            $table->unsignedBigInteger('imovel_id')->unique();
            $table->timestamps();
            $table->foreign('locatario_id')->references('id')->on('locatarios');
            $table->foreign('imovel_id')->references('id')->on('imoveis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aluguels');
    }
}
