<?php

namespace Database\Factories;

use App\Models\Imovel;
use App\Models\Locador;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImovelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Imovel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $locadores = Locador::pluck('id')->toArray();
        return [
            'escriture' => $this->faker->numberBetween(10000, 2000000),
            'cep' => $this->faker->postcode(),
            'uf' => $this->faker->stateAbbr(),
            'city' => $this->faker->citySuffix(),
            'address' => $this->faker->address(),
            'size' => $this->faker->numberBetween(30, 400),
            'locador_id' => $this->faker->randomElement($locadores)
        ];
    }
}
