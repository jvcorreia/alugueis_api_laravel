<?php

namespace Database\Factories;

use App\Models\Imovel;
use App\Models\Photo;
use Illuminate\Database\Eloquent\Factories\Factory;

class PhotoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Photo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {   $filePath = storage_path('img');
        $imoveis = Imovel::pluck('id')->toArray();
        return [
            'imovel_id' =>  $this->faker->randomElement($imoveis),
            'path' => $this->faker->image($filePath, $width = 640, $height = 480),
            'description' => $this->faker->paragraph($nbSentences = 1, $variableNbSentences = true)
        ];
    }
}
