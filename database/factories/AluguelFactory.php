<?php

namespace Database\Factories;

use App\Models\Aluguel;
use App\Models\Imovel;
use App\Models\Locador;
use App\Models\Locatario;
use Illuminate\Database\Eloquent\Factories\Factory;

class AluguelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Aluguel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'price' => $this->faker->randomFloat(),
            'location_type' => $this->faker->numberBetween(1, 3),
            'locatario_id' => $this->faker->unique()->numberBetween(1, Locatario::all()->count()),
            'imovel_id' => $this->faker->unique()->numberBetween(1, Imovel::all()->count()),
        ];
    }
}
