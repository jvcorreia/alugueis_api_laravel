<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users', [\App\Http\Controllers\UserController::class, 'index']);
Route::get('/users/user/{id}', [\App\Http\Controllers\UserController::class, 'show']);
Route::put('/users/update/{id}', [\App\Http\Controllers\UserController::class, 'update']);
Route::delete('/users/delete/{id}', [\App\Http\Controllers\UserController::class, 'destroy']);
Route::post('/users/new', [\App\Http\Controllers\UserController::class, 'store']);

Route::get('/locadores', [\App\Http\Controllers\LocadorController::class, 'index']);
Route::get('/locadores/locador/{id}', [\App\Http\Controllers\LocadorController::class, 'show']);
Route::put('/locadores/update/{id}', [\App\Http\Controllers\LocadorController::class, 'update']);
Route::delete('/locadores/delete/{id}', [\App\Http\Controllers\LocadorController::class, 'destroy']);
Route::post('/locadores/new', [\App\Http\Controllers\LocadorController::class, 'store']);


Route::get('/locatarios', [\App\Http\Controllers\LocatarioController::class, 'index']);
Route::get('/locatarios/locatario/{id}', [\App\Http\Controllers\LocatarioController::class, 'show']);
Route::put('/locatarios/update/{id}', [\App\Http\Controllers\LocatarioController::class, 'update']);
Route::delete('/locatarios/delete/{id}', [\App\Http\Controllers\LocatarioController::class, 'destroy']);
Route::post('/locatarios/new', [\App\Http\Controllers\LocatarioController::class, 'store']);

Route::get('/imoveis', [\App\Http\Controllers\ImovelController::class, 'index']);
Route::get('/imoveis/imovel/{id}', [\App\Http\Controllers\ImovelController::class, 'show']);
Route::put('/imoveis/update/{id}', [\App\Http\Controllers\ImovelController::class, 'update']);
Route::delete('/imoveis/delete/{id}', [\App\Http\Controllers\ImovelController::class, 'destroy']);
Route::post('/imoveis/new', [\App\Http\Controllers\ImovelController::class, 'store']);

Route::get('/alugueis', [\App\Http\Controllers\AluguelController::class, 'index']);
Route::get('/alugueis/aluguel/{id}', [\App\Http\Controllers\AluguelController::class, 'show']);
Route::put('/alugueis/update/{id}', [\App\Http\Controllers\AluguelController::class, 'update']);
Route::delete('/alugueis/delete/{id}', [\App\Http\Controllers\AluguelController::class, 'destroy']);
Route::post('/alugueis/new', [\App\Http\Controllers\AluguelController::class, 'store']);

Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login']);
Route::post('/logout', [\App\Http\Controllers\AuthController::class, 'logout']);
Route::post('/refresh', [\App\Http\Controllers\AuthController::class, 'refresh']);
Route::post('/me', [\App\Http\Controllers\AuthController::class, 'me']);

Route::fallback(function (){
    return response()->json(['erro'=>'Essa rota não existe em nosso site!'], 404);
});
