<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aluguel extends Model
{
    use HasFactory;
    protected $table = 'alugueis';
    protected $fillable = ['price', 'location_type', 'imovel_id', 'locatario_id'];

    public function imovel()
    {
        return $this->belongsTo(Imovel::class);
    }
    public function locatario()
    {
        return $this->belongsTo(Locatario::class);
    }
}
