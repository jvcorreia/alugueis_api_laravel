<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Locatario extends Model
{
    use HasFactory;
    protected $table = 'locatarios';

    protected $fillable = [
        'name',
        'email',
        'password',
        'uf'
    ];
}
