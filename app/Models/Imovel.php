<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Imovel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'imoveis';
    protected $fillable = ['escriture', 'cep', 'uf', 'city', 'address', 'size', 'locador_id'];

   public function locador()
   {
       return $this->belongsTo(Locador::class, 'locador_id');
   }

   public function photos()
   {
       return $this->hasMany(Photo::class);
   }
}
