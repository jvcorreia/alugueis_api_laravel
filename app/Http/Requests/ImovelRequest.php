<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImovelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'escriture' => 'required|unique:imoveis|numeric',
            'cep' => 'required',
            'uf' => 'required|max:2|min:2',
            'address' => 'required',
            'size' => 'numeric|required',
            'locador_id' => 'exists:locadores,id|required'
        ];
    }

    public function messages()
    {
        return ['required' => 'O campo :attribute é obrigatório.'];
    }
}
