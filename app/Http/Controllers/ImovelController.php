<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImovelRequest;
use App\Models\Imovel;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImovelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Imovel::with(['locador', 'photos'])->paginate(5), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImovelRequest $request)
    {
        $dados = $request->only('escriture', 'cep','city', 'uf', 'address', 'size', 'locador_id');

        $imovel = Imovel::create($dados);

        if ($request->photo !== null) {
            $image = base64_decode($request->photo);
            $safeName = uniqid().'.'.'png';
            \File::put(storage_path(). '/img/' . $safeName, $image);
            Photo::create([
                'path' => storage_path(). '/img/' . $safeName,
                'description' => $request->description,
                'imovel_id' => $imovel->id
            ]);

            return response()->json(Imovel::with('photos', 'locador')->where('id', $imovel->id)->get(), 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $imovel = Imovel::find($id);

        if ($imovel === null) {
            return response()->json(['errors' => 'O id de imovel digitado não existe'], 404);
        }
        return response()->json(Imovel::with('photos', 'locador')->where('id', $id)->get(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ImovelRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $imovel = Imovel::find($id);

        if ($imovel === null) {
            return response()->json(['erro' => 'Imóvel informado não existe'], 401);
        }

        if ($imovel->photos()->count() > 0) {
            $imovel->photos()->delete();
            $imovel->delete();
            return response()->json(['message' => 'Imóvel deletado com sucesso.'], 200);
        }

        $imovel->delete();
        return response()->json(['message' => 'Imóvel deletado com sucesso.'], 200);
    }
}
