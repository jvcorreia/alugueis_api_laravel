<?php

namespace App\Http\Controllers;

use App\Models\Locador;
use Illuminate\Http\Request;

class LocadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Locador::with('imovel')->get(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dados = $request->all();
        $dados['password'] = bcrypt($dados['password']);
        $regras = [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'uf' => 'required'
        ];

        $feedback = [
            'required' => 'O campo :attribute é obrigatório.'
        ];

        $request->validate($regras, $feedback);

        $locador = Locador::create($dados);

        return response()->json($locador, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $locador = Locador::find($id)->imovel();

        if ($locador === null) {
            return response()->json(['erro' => 'Locador não existe'], 404);
        }

        return response()->json($locador, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $locador = Locador::find($id);

        if ($locador === null) {
            return response()->json(['erro' => 'Usuário não existe'], 404);
        }
        $regras = [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'uf' => 'required'
        ];

        $feedback = [
            'required' => 'O campo :attribute é obrigatório.'
        ];

        $request->validate($regras, $feedback);
        $locador->update($request->all());
        return response()->json($locador, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $locador = Locador::find($id);

        if ($locador === null) {
            return response()->json(['erro' => 'Usuário não existe'], 404);
        }

        if ($locador->imovel()->count() > 0) {
            return response()->json(['erro' => 'O locador possui imóveis vinculados'], 401);
        }

        $locador->delete();
        return response()->json(['message' => 'Locador deletado com sucesso'], 200);

    }
}
