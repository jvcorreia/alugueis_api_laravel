<?php

namespace App\Http\Controllers;

use App\Models\Locatario;
use Illuminate\Http\Request;

class LocatarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Locatario::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dados = $request->all();
        $dados['password'] = bcrypt($dados['password']);
        $regras = [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'uf' => 'required'
        ];

        $feedback = [
            'required' => 'O campo :attribute é obrigatório.'
        ];

        $request->validate($regras, $feedback);

        $locatario = Locatario::create($dados);

        return response()->json($locatario, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $locatario = Locatario::find($id);

        if ($locatario === null) {
            return response()->json(['erro' => 'Locatario não existe'], 404);
        }

        return response()->json($locatario, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $locatario = Locatario::find($id);

        if ($locatario === null) {
            return response()->json(['erro' => 'Locatário não existe'], 404);
        }
        $regras = [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'uf' => 'required'
        ];

        $feedback = [
            'required' => 'O campo :attribute é obrigatório.'
        ];

        $request->validate($regras, $feedback);
        $locatario->update($request->all());
        return response()->json($locatario, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $locatario = Locatario::find($id);

        if ($locatario === null) {
            return response()->json(['erro' => 'Locatário não existe'], 404);
        }

        $locatario->delete();
        return response()->json(['message' => 'Locatário deletado com sucesso'], 200);
    }
}
